# ------------------------------------------------------
# ---------------------- main.py -----------------------
# ------------------------------------------------------
from PyQt5.QtWidgets import*
from PyQt5.uic import loadUi
from  matplotlib.backends.backend_qt5agg  import  ( NavigationToolbar2QT  as  NavigationToolbar )
from matplotlib.patches import Arc
from pandas.io.json import json_normalize
from os import listdir
import os
import pandas as pd
import matplotlib.patches as patches
import json


class ListMatchesDialogWidget(QDialog):

    def __init__(self, parent=None):

        super().__init__(parent)

        loadUi("select_match_dialog.ui",self)
        self.accepted = False

    def setList(self, matchesList):
        self.listWidget_matches.addItems(matchesList)

    def getCurrentMatch(self):
        match_id_input = self.listWidget_matches.currentItem().text()
        match_id = match_id_input.split()[0]
        return match_id


class MatplotlibWidget(QMainWindow):

    def __init__(self):

        QMainWindow.__init__(self)

        loadUi("mainwindow.ui",self)

        self.setWindowTitle("Football Matches Data Visualisation GUI")
        # maximize the window:
        self.showMaximized()

        self.pushButton_load_match.clicked.connect(self.load_match)
        self.pushButton_shot_maps.clicked.connect(self.load_shot_maps)
        self.pushButton_pass_maps.clicked.connect(self.load_pass_maps)
        # other buttons as disabled until a match has been loaded:
        self.pushButton_shot_maps.setEnabled(False)
        self.pushButton_pass_maps.setEnabled(False)
        # set time of match to display:
        self.pushButton_half1.clicked.connect(self.setTimeFirstHalf)
        self.pushButton_half2.clicked.connect(self.setTimeSecondHalf)
        self.pushButton_reset.clicked.connect(self.setTimeFullMatch)

        for checkbox in self.groupBox.findChildren(QRadioButton):
            checkbox.toggled.connect(self.homeAwayToggled)
            checkbox.setEnabled(False)

        self.comboBox_players.activated.connect(self.generate_visualisations)
        self.comboBox_filter.activated.connect(self.generate_visualisations)
        self.horizontalSlider_start.sliderReleased.connect(self.generate_visualisations)
        self.horizontalSlider_end.sliderReleased.connect(self.generate_visualisations)
        # built in matplotlib toolbar:
        self.addToolBar(NavigationToolbar(self.MplWidget.canvas, self))

        self.match_id = None
        self.df_events = None
        self.config = read_json("config.json")
        self.height = float(self.config["height"])
        self.width = float(self.config["width"])
        self.current_vis_type = "None"

    def get_home_away(self):
        for checkbox in self.groupBox.findChildren(QRadioButton):
            if (checkbox.isChecked()):
                return checkbox.text()

        return "Home"
    def setTimeFirstHalf(self):
        self.horizontalSlider_start.setValue(0)
        self.horizontalSlider_end.setValue(45)
        self.generate_visualisations()
    def setTimeSecondHalf(self):
        self.horizontalSlider_end.setValue(90)
        self.horizontalSlider_start.setValue(45)
        self.generate_visualisations()
    def setTimeFullMatch(self):
        self.horizontalSlider_end.setValue(90)
        self.horizontalSlider_start.setValue(0)
        self.generate_visualisations()
    def draw_pitch(self):
        # draws a pitch with matplotlib
        height = self.height
        width = self.width
        background_color = self.config["background_color"]
        lines_color = self.config["lines_color"]
        fig_size_x = self.config["fig_size"]
        fig_size_y = fig_size_x * (height/width)

        self.MplWidget.canvas.axes.clear()

        # set axis limits:
        self.MplWidget.canvas.axes.set_xlim([0,width])
        self.MplWidget.canvas.axes.set_ylim([0,height])

        # fill in rectangle shape of pitch:
        self.MplWidget.canvas.axes.add_patch(patches.Rectangle((0, 0), width, height, color=background_color))

        # outline of pitch:
        self.MplWidget.canvas.axes.plot((0,width,width,0,0),(0,0,height,height,0),color=lines_color, lw=1.5)
        # halfway line:
        self.MplWidget.canvas.axes.plot((width/2,width/2),(0,height),color=lines_color,lw=1.5)
        # left penalty area:
        box_start_y = (height-44)/2
        self.MplWidget.canvas.axes.plot((0,18,18,0,0),(box_start_y,box_start_y,box_start_y+44,box_start_y+44,box_start_y),color=lines_color,lw=1.5)
        # right penalty area:
        self.MplWidget.canvas.axes.plot((width,width-18,width-18,width,width),(box_start_y,box_start_y,box_start_y+44,box_start_y+44,box_start_y),
                                                                                                color=lines_color,lw=1.5)
        # 6 yard box:
        box_start_y += 12
        self.MplWidget.canvas.axes.plot((0,6,6,0,0),(box_start_y,box_start_y,box_start_y+20,box_start_y+20,box_start_y),color=lines_color,lw=1)
        self.MplWidget.canvas.axes.plot((width,width-6,width-6,width,width),(box_start_y,box_start_y,box_start_y+20,box_start_y+20,box_start_y),
                                                                                                color=lines_color,lw=1)
        # center circle:
        self.MplWidget.canvas.axes.add_patch(patches.Wedge((width/2, height/2), 10, 0, 360, fill=True, edgecolor=lines_color,
                                   facecolor=lines_color, zorder=4, width=0.02, alpha=0.8))

        # arcs outside penalty areas:
        self.MplWidget.canvas.axes.add_patch(Arc((12,40),height=20,width=20,angle=0,theta1=308,theta2=52,color="white"))
        self.MplWidget.canvas.axes.add_patch(Arc((width-12,40),height=20,width=20,angle=180,theta1=308,theta2=52,color="white"))

        self.MplWidget.canvas.axes.axis('off')
        self.MplWidget.canvas.axes.axis('scaled')
        self.MplWidget.canvas.draw()
    def plot_passes(self,df_passes):
        background_color = self.config["background_color"]
        for i, df_pass in df_passes.iterrows():

            origin_x = df_pass["origin_pos_x"]
            origin_y = df_pass["origin_pos_y"]

            end_x = df_pass["end_pos_x"]
            end_y = df_pass["end_pos_y"]

            marker_size = 10
            node_color = "#3498DB"

            if (df_pass["height"] == "High Pass"):
                node_color = "#F1C40F"

            if (df_pass['outcome'] == "Incomplete"):
                node_color = "#CB4335"

            # alpha sets the transparency of the markers
            self.MplWidget.canvas.axes.plot(origin_x, origin_y, '.', color=node_color, markersize=marker_size, zorder=5, alpha=.5)

            self.MplWidget.canvas.axes.arrow(origin_x,origin_y,end_x-origin_x,end_y-origin_y,color=node_color,width=0.2,
                                            head_width = 1,length_includes_head = True, alpha=.5)

        lp = patches.Patch(color = "#3498DB", label = "Low/Ground Pass")
        hp = patches.Patch(color = "#F1C40F", label = "High Pass")
        ip = patches.Patch(color = "#CB4335", label = "Incomplete Pass")

        self.MplWidget.canvas.axes.legend(handles = [lp,hp,ip], frameon = False, loc = "center left",
                         bbox_to_anchor=(0.043,0.15), prop={'size': 12}, labelcolor='white')
    def plot_shots(self,df_shots):
        background_color = self.config["background_color"]
        max_xg = 0.99
        for i, shot in df_shots.iterrows():

            shot_x = shot["shot_pos_x"]
            shot_y = shot["shot_pos_y"]

            shot_xg=shot['shot_xg']

            marker_size = _change_range(shot_xg, (0, max_xg), (self.config["min_node_size"], self.config["max_node_size"]))
            node_color = "#F1C40F"
            legend_label = "Unsuccessful Shot"

            if (shot['outcome_name'] == 'Goal'):
                node_color = "#229954"
                legend_label = "Goal"

            # alpha sets the transparency of the markers
            self.MplWidget.canvas.axes.plot(shot_x, shot_y, '.', color=node_color, markersize=marker_size, zorder=5, alpha=.5, label=legend_label)


        g = patches.Patch(color = "#229954", label = "Goal")
        ms = patches.Patch(color = "#F1C40F", label = "Missed Shot")


        marker_size = _change_range(0.1, (0, max_xg), (self.config["min_node_size"], self.config["max_node_size"]))
        self.MplWidget.canvas.axes.plot(38,8,'.',color="#F1C40F",markersize=marker_size)
        marker_size = _change_range(0.25, (0, max_xg), (self.config["min_node_size"], self.config["max_node_size"]))
        self.MplWidget.canvas.axes.plot(43,8,'.',color="#F1C40F",markersize=marker_size)
        marker_size = _change_range(0.5, (0, max_xg), (self.config["min_node_size"], self.config["max_node_size"]))
        self.MplWidget.canvas.axes.plot(50,8,'.',color="#F1C40F",markersize=marker_size)
        self.MplWidget.canvas.axes.arrow(40,13,12,0,color="white",width=0.2,head_width = 1,length_includes_head = True)
        self.MplWidget.canvas.axes.text(37,12,'xG',color="white",horizontalalignment="center")


        self.MplWidget.canvas.axes.legend(handles = [g,ms], frameon = False, loc = "center left",
                         bbox_to_anchor=(0.043,0.15), prop={'size': 12}, labelcolor='white')
    def run_shot_maps(self,filter):
        print("Running Shot Maps")
        df_events = self.df_events
        height = self.height
        width = self.width
        team1 = getTeamNames(df_events)[0]
        team2 = getTeamNames(df_events)[1]

        homeAway = self.get_home_away()
        team_name = home_or_away(team1,team2,homeAway)[0]
        team_lineup = getTeamLineup(df_events,team_name,home_or_away(team1,team2,homeAway)[1])
        opponent_team = [x for x in df_events.team_name.unique() if x != team_name][0]

        vis_type = self.comboBox_filter.currentText()
        if (vis_type == "All Shots"):
            vis_type = ""
        else:
            vis_type = " (" + vis_type + ") "

        vis_subject = team_name

        player_name = self.comboBox_players.currentText()
        if (player_name != "All"):
            player_id = int(team_lineup[team_lineup['player_name'] == player_name]['player_id'])
            player_number = int(team_lineup[team_lineup['player_name'] == player_name]['jersey_number'])
            # filter df_passes to the specific player of choice:
            df_player_events = df_events[(df_events.player_id == player_id)].copy()
            vis_subject = player_name
            title = "Shot Map{0} - {1} shots vs {2}".format(vis_type, vis_subject, opponent_team)
        else:
            title = "Shot Map{0} - {1} shots vs {2}".format(vis_type, vis_subject, opponent_team)
            df_player_events = df_events.copy()
            if (homeAway == "Both"):
                title = "Shot Map{0} - {1} vs {2}".format(vis_type, vis_subject, opponent_team)

        self.draw_pitch()

        # filter for only events within user's selected time of the game:
        df_player_events = self.filterEventsMinutes(df_player_events)

        if (homeAway == "Both"):
            # TEAM 1:
            df_shots = read_shots(df_player_events,team1,filter)
            self.plot_shots(df_shots)
            # TEAM 2:
            df_shots = read_shots(df_player_events,team2,filter)
            df_shots = flip_shots(df_shots, self.width, self.height)
            self.plot_shots(df_shots)
        elif (homeAway == "Away"):
            df_shots = read_shots(df_player_events, team_name,filter)
            df_shots = flip_shots(df_shots, self.width, self.height)
            self.plot_shots(df_shots)
        else:
            df_shots = read_shots(df_player_events, team_name,filter)
            self.plot_shots(df_shots)

        # ANNOTATIONS:
        # Team 1:
        self.MplWidget.canvas.axes.arrow(width/2 + 3,2,width/2-5,0,color="white",width=0.2,head_width = 1,length_includes_head = True)
        self.MplWidget.canvas.axes.text(width*0.75,4,team1,color="white",horizontalalignment="center")

        # Team 2:
        self.MplWidget.canvas.axes.arrow(width/2 - 3,height-2,-(width/2-5),0,color="white",width=0.2,head_width = 1,length_includes_head = True)
        self.MplWidget.canvas.axes.text(width*0.25,height-6,team2,color="white",horizontalalignment="center")

        title = self.getMinutesTitle(title)
        self.MplWidget.canvas.axes.set_title(title, loc="center")

        self.MplWidget.canvas.draw()
    def run_pass_maps(self,filter):
        print("Running Pass Maps")
        df_events = self.df_events
        team1 = getTeamNames(df_events)[0]
        team2 = getTeamNames(df_events)[1]
        homeAway = self.get_home_away()
        team_name = home_or_away(team1,team2,homeAway)[0]
        opponent_team = [x for x in df_events.team_name.unique() if x != team_name][0]
        team_lineup = getTeamLineup(df_events,team_name,home_or_away(team1,team2,homeAway)[1])

        vis_subject = team_name

        # filter for only events within user's selected time of the game:
        df_events = self.filterEventsMinutes(df_events)

        # could insert if statements to read the correct type of passes...
        if (filter == "chances"):
            df_passes_refined = read_pre_xG_passes(df_events,team_name)
        elif (filter == "crosses"):
            df_passes_refined = read_crosses(df_events,team_name)
        elif (filter == "through balls"):
            df_passes_refined = read_through_balls(df_events,team_name)
        elif (filter == "none"):
            df_passes_refined = read_passes(df_events, team_name)

        # filter for individual players:
        player_name = self.comboBox_players.currentText()
        if (player_name != "All"):
            player_id = int(team_lineup[team_lineup['player_name'] == player_name]['player_id'])
            player_number = int(team_lineup[team_lineup['player_name'] == player_name]['jersey_number'])
            # filter df_passes to the specific player of choice:
            df_passes_refined = df_passes_refined[(df_passes_refined.player_id == player_id)].copy()
            vis_subject = player_name


        self.draw_pitch()
        self.plot_passes(df_passes_refined)

        self.MplWidget.canvas.axes.arrow(self.width/2 + 3,-2,self.width/2-5,0,color="black",width=0.2,head_width = 1,length_includes_head = True, clip_on = False)
        self.MplWidget.canvas.axes.text(self.width*0.75,-6,team_name,color="black",horizontalalignment="center")

        vis_type = self.comboBox_filter.currentText()
        if (vis_type == "All Passes"):
            vis_type = ""
        else:
            vis_type = " (" + vis_type + ") "

        title = "Pass Map{0} - {1} Passes vs {2}".format(vis_type, vis_subject, opponent_team)
        title = self.getMinutesTitle(title)
        self.MplWidget.canvas.axes.set_title(title, loc="center")

        self.MplWidget.canvas.draw()
    def load_shot_maps(self):
        for checkbox in self.groupBox.findChildren(QRadioButton):
                checkbox.setEnabled(True)

        if (self.current_vis_type != "ShotMap"):
            self.comboBox_filter.clear()
            self.comboBox_filter.addItem("All Shots")
            self.comboBox_filter.addItem("Shots from open play")
            self.comboBox_filter.addItem("Shots from free kick")
            self.comboBox_filter.addItem("Headers")
            self.comboBox_filter.addItem("Blocked")
            self.current_vis_type = "ShotMap"

        vis_type = self.comboBox_filter.currentText()
        if (vis_type == "All Shots"):
            self.run_shot_maps("none")
        elif (vis_type == "Shots from open play"):
            self.run_shot_maps("open play")
        elif (vis_type == "Blocked"):
            self.run_shot_maps("blocked")
        elif (vis_type == "Headers"):
            self.run_shot_maps("headers")
        elif (vis_type == "Shots from free kick"):
            self.run_shot_maps("free kicks")
    def load_pass_maps(self):
        if (self.current_vis_type != "PassMap"):
            self.comboBox_filter.clear()
            self.comboBox_filter.addItem("All Passes")
            self.comboBox_filter.addItem("Passes in build up to big chances")
            self.comboBox_filter.addItem("Crosses")
            self.comboBox_filter.addItem("Through balls")
            self.current_vis_type = "PassMap"

            for checkbox in self.groupBox.findChildren(QRadioButton):
                if (checkbox.text() == "Both"):
                    checkbox.setEnabled(False)
                    if checkbox.isChecked():
                        for x in self.groupBox.findChildren(QRadioButton):
                            if (x.text() == "Home"):
                                x.setChecked(True)

        vis_type = self.comboBox_filter.currentText()
        if (vis_type == "All Passes"):
            self.run_pass_maps("none")
        elif (vis_type == "Passes in build up to big chances"):
            self.run_pass_maps("chances")
        elif (vis_type == "Crosses"):
            self.run_pass_maps("crosses")
        elif (vis_type == "Through balls"):
            self.run_pass_maps("through balls")
    def load_players(self):
        df_events = self.df_events
        team1 = getTeamNames(df_events)[0]
        team2 = getTeamNames(df_events)[1]
        homeAway = self.get_home_away()
        team_name = home_or_away(team1,team2,homeAway)[0]
        team_lineup = getTeamLineup(df_events,team_name,home_or_away(team1,team2,homeAway)[1])
        # get list of names from dataframe to show in comboBox
        players_list = team_lineup['player_name'].tolist()
        self.comboBox_players.clear()
        self.comboBox_players.addItem("All")
        self.comboBox_players.addItems(players_list)
        self.comboBox_players.setCurrentText("All")
    def load_match(self):
        # creates a dialog to display list of matches...
        filesList = [f for f in listdir("data/events/")]
        matchesTitles = []

        # Reads the lineup file of every match to find the team names:
        for file in filesList:
            matchid = os.path.splitext(file)[0]
            lineup_events = read_json("data/lineups/{0}.json".format(matchid))
            df_lineups = json_normalize(lineup_events, sep="_").assign(match_id=matchid)
            for i, df_lineup in df_lineups.iterrows():
                if (i==0):
                    team1 = df_lineup["team_name"]
                else:
                    team2 = df_lineup["team_name"]

            matchTitle = ("{} ({} vs {})".format(matchid,team1,team2))
            matchesTitles.append(matchTitle)

        # Creates an instance of ListMatchesDialogWidget
        dialog = ListMatchesDialogWidget(self)
        dialog.setList(matchesTitles)
        dialog.setWindowTitle("Select Match")
        dialog.show()
        # If the user has selected a match
        if (dialog.exec()):
            self.match_id = dialog.getCurrentMatch()
            self.df_events = read_events(self.match_id)

            self.draw_pitch()
            team1 = getTeamNames(self.df_events)[0]
            team2 = getTeamNames(self.df_events)[1]
            self.MplWidget.canvas.axes.set_title("{} vs {}".format(team1,team2))
            self.MplWidget.canvas.draw()
            self.load_players()
            self.pushButton_shot_maps.setEnabled(True)
            self.pushButton_pass_maps.setEnabled(True)
            for checkbox in self.groupBox.findChildren(QRadioButton):
                checkbox.setEnabled(True)
    def generate_visualisations(self):
        # Function to be ran whenever the current visualisation needs to be changed or updated
        if (self.current_vis_type == "ShotMap"):
            self.load_shot_maps()
        elif (self.current_vis_type == "PassMap"):
            self.load_pass_maps()
    def homeAwayToggled(self):
        # Ensures visualisations are updated whenever the home/away option is toggled
        self.load_players()
        self.generate_visualisations()
    def filterEventsMinutes(self, df_events):
        # Checks the values of the start/end sliders and filters to the events within the selected timeframe
        start = self.horizontalSlider_start.value()
        end = self.horizontalSlider_end.value()
        df_events = df_events[(df_events.minute <= end)].copy()
        df_events = df_events[(df_events.minute >= start)].copy()
        return df_events
    def getMinutesTitle(self,title):
        # Checks to see if any extra information regarding the timeframe should be appended to the visualisation title
        start = self.horizontalSlider_start.value()
        end = self.horizontalSlider_end.value()
        if (start == 0 and end == 90):
            return title
        elif (start == 0 and end == 45):
            return title + " (First Half)"
        elif (start == 45 and end == 90):
            return title + " (Second Half)"
        else:
            return title + " (From minutes " + str(start) + "-" + str(end) + ")"

def read(path):
    # Reads the content of a file
    with open(path, 'r') as f:
        return f.read()
def read_json(path):
    # Reads the content of a JSON file
    return json.loads(read(path))
def _change_range(value, old_range, new_range):
    '''
    Convert a value from one range to another one, maintaining ratio.
    '''
    return ((value-old_range[0]) / (old_range[1]-old_range[0])) * (new_range[1]-new_range[0]) + new_range[0]
def read_events(match_id):
    events = read_json("data/events/{0}.json".format(match_id))
    # events dataframe:
    df_events = json_normalize(events, sep="_").assign(match_id=match_id)

    return df_events
def getTeamNames(df_events):
    df_events_teams = df_events[(df_events.type_name == "Starting XI")].copy()
    team1 = df_events_teams.team_name[0]
    team2 = df_events_teams.team_name[1]

    return team1, team2
def getTeamLineup(df_events, team_name, homeAway):
    df_events_lineups= df_events[(df_events.type_name == "Starting XI") &
                              (df_events.team_name == team_name)].copy()

    df_lineups = json_normalize(df_events_lineups.tactics_lineup[homeAway], sep="_")
    return df_lineups
def home_or_away(team1,team2,homeAway):
    if (homeAway == "Home"):
        return team1, 0
    elif (homeAway == "Away"):
        return team2, 1
    else:
        # when it's both, return home team by default
        return team1, 0
def flip_shots(df_shots, width, height):
    # This function changes locations of shots from left->right to right->left & vice versa (used for plotting away team shots)
    df_shots["shot_pos_x"] = df_shots.shot_pos_x.apply(lambda x: width-x)
    df_shots["shot_pos_y"] = df_shots.shot_pos_y.apply(lambda y: height-y)
    return df_shots
def read_shots(df_events,team_name,filter):
    df_all_shots = df_events[(df_events.type_name == "Shot") &
                              (df_events.team_name == team_name)].copy()

    if (filter == "open play"):
        df_filtered_shots = df_all_shots[(df_all_shots.shot_type_name == "Open Play")].copy()
    elif (filter == "free kicks"):
        df_filtered_shots = df_all_shots[(df_all_shots.shot_type_name == "Free Kick")].copy()
    elif (filter == "blocked"):
        df_filtered_shots = df_all_shots[(df_all_shots.shot_outcome_name == "Blocked")].copy()
    elif (filter == "headers"):
        df_filtered_shots = df_all_shots[(df_all_shots.shot_body_part_name == "Head")].copy()
    elif (filter == "none"):
        df_filtered_shots = df_all_shots.copy()

    # Creates a new dataframe with just 3 columns to start with (index, shot_pos_x, shot_pos_y)
    df_shots = pd.DataFrame(df_filtered_shots.location.to_list(), columns=['shot_pos_x', 'shot_pos_y'], index=df_filtered_shots.index)

    # ADD HERE ANY OTHER COLUMNS TO POPULATE THE DATAFRAME WITH
    # ---------------------------------------------------------
    df_shots["shot_xg"] = df_filtered_shots.shot_statsbomb_xg
    df_shots["outcome_id"] = df_filtered_shots.shot_outcome_id
    df_shots["outcome_name"] = df_filtered_shots.shot_outcome_name
    # ---------------------------------------------------------
    return df_shots
def read_passes(df_events,team_name):
    df_events_passes = df_events[(df_events.type_name == "Pass") &
                              (df_events.team_name == team_name)].copy()

    # Creates a new dataframe with just 3 columns to start with (index, shot_pos_x, shot_pos_y)
    df_passes_origin = pd.DataFrame(df_events_passes.location.to_list(), columns=['origin_pos_x', 'origin_pos_y'],
                                                                             index=df_events_passes.index)

    # creates new dataframe with end locations of passes:
    df_passes_end = pd.DataFrame(df_events_passes.pass_end_location.to_list(), columns=['end_pos_x', 'end_pos_y'],
                                                                             index=df_events_passes.index)

    # dataframe merging start & end locations of passes:
    df_passes = pd.merge(df_passes_origin, df_passes_end, left_index=True, right_index=True)

    # ADD HERE ANY OTHER COLUMNS TO POPULATE THE DATAFRAME WITH
    # ---------------------------------------------------------
    df_passes["outcome"] = df_events_passes.pass_outcome_name
    df_passes["player_id"] = df_events_passes.player_id
    df_passes["height"] = df_events_passes.pass_height_name
    df_passes["recipient_id"] = df_events_passes.pass_recipient_id
    # ---------------------------------------------------------
    return df_passes
def read_pre_xG_passes(df_events,team_name):
    # dataframe containing all passes, and shots with xg > 0.1:
        df_passes_chances = df_events[((df_events.type_name == "Shot") & (df_events.shot_statsbomb_xg > 0.1 ) &
                                  (df_events.team_name == team_name))
                                | (df_events.type_name == "Pass") &
                                  (df_events.team_name == team_name)].copy()

        df_passes_chances = df_passes_chances.reset_index()
        # Create an empty dataframe with same columns as df_events:
        df_buildup_passes = pd.DataFrame(columns=df_events.columns)

        for i, event in df_passes_chances.iterrows():
            if (event.type_name == "Shot"):
                possession = 1
                for x in range(1,11):
                    # 10 most recent passes leading up to the shot
                    # check if there were any turnovers in possession within those 10 passes
                    if (i-x > 0) & (possession==1):
                        current_pass_event_id = df_passes_chances.loc[i-x]["index"]
                        previous_pass_event_id = df_passes_chances.loc[i-x-1]["index"]

                        df_row = df_events[df_events["index"]==current_pass_event_id]
                        df_buildup_passes = df_buildup_passes.append(df_row)

                        for y in range(previous_pass_event_id,current_pass_event_id):
                            if (df_events.loc[y].possession_team_name != team_name):
                                # if true then there must've been a possession turnover
                                possession = 0

        # remove any duplicate values:
        df_buildup_passes = df_buildup_passes.loc[~df_buildup_passes.index.duplicated(), :]
        # df_buildup_passes contains all passes leading to big chances

        # remove unneccessary columns:
        df_passes_refined = read_passes(df_buildup_passes,team_name)
        return df_passes_refined
def read_crosses(df_events,team_name):
    df_events_passes = df_events[(df_events.type_name == "Pass") &
                              (df_events.team_name == team_name)].copy()

    df_crosses = pd.DataFrame(columns=df_events_passes.columns)

    if 'pass_cross' in df_events_passes:
        df_crosses = df_events_passes[(df_events_passes.pass_cross == True)].copy()

    # still  run read_passes to filter out columns that are not needed
    df_passes_crosses = read_passes(df_crosses,team_name)
    return  df_passes_crosses
def read_through_balls(df_events,team_name):
    # a through ball is a pass that cuts the last line of defence
    df_passes = df_events[(df_events.type_name == "Pass") &
                              (df_events.team_name == team_name)].copy()

    df_through_balls = pd.DataFrame(columns=df_passes.columns)

    if 'pass_through_ball' in df_passes:
        df_through_balls = df_passes[(df_passes.pass_through_ball == True)].copy()

    # still  runs read_passes to filter out columns that are not needed
    df_passes_through_balls = read_passes(df_through_balls,team_name)
    return  df_passes_through_balls




app = QApplication([])
window = MatplotlibWidget()
window.show()
app.exec_()
